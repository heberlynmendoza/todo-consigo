#!/bin/bash

# shellcheck disable=SC2034
RED="\033[1;31m"
GREEN="\033[1;32m"
NOCOLOR="\033[0m"

final_msg="@(^_^) ============= finished process ============= (^_^)!"
option=''

echo \ &&
echo -e "${GREEN}what do you want to run?${NOCOLOR}"
echo -e "- All: ${GREEN}[1]${NOCOLOR}"
echo -e "- Install Plugins: ${GREEN}[2]${NOCOLOR}"
echo -e "- Update wp-config.php: ${GREEN}[3]${NOCOLOR}"
echo -e "- Clean debug.log: ${GREEN}[4]${NOCOLOR}"
echo -e "- Import DB: ${GREEN}[5]${NOCOLOR}"
echo -e "- Backup DB: ${GREEN}[]${NOCOLOR}"
echo \

# shellcheck disable=SC2162
read -p 'Type your option: ' option

if [[ $option == 1 ]]; then
  # shellcheck disable=SC2164
  cd wp

  touch 'wp-content/'debug.log
  cp .env.example .env

  docker-compose up -d

  # DB
  echo \ && echo -e "${GREEN}""1 ---- Import DB""${NOCOLOR}"
  docker exec -i bk_mysql sh -c "chmod +x /tmp/db/import.sh"
  docker exec -i bk_mysql sh -c "/tmp/db/import.sh"

  # Set permissions
  echo \ && echo -e "${GREEN}""2 ---- Set permissions""${NOCOLOR}"
  docker exec -i wordpress sh -c 'chown www-data:www-data -R wp-content'

  # Wordpress setup
  echo \ && echo -e "${GREEN}""3 ---- Wordpress Setup""${NOCOLOR}"

  echo -e "${RED}""––>3.1 Checking plugins list ...""${NOCOLOR}"
  docker exec -i wpcli sh -c "chmod +x plugins/managePluginsDev.sh"
  docker exec -i wpcli sh -c "plugins/managePluginsDev.sh"

  echo -e "${RED}""––>3.1 Rewrite flush""${NOCOLOR}"
  docker exec -i wpcli sh -c 'wp rewrite flush'

  # shellcheck disable=SC2164
  cd app/;

  # Composer
  echo \ && echo -e "${GREEN}""4 ---- Composer""${NOCOLOR}"
  composer --version > /dev/null 2>& 1 && echo 'composer is installed'; composer install || echo "composer isn't installed"

  # NPM
  echo \ && echo -e "${GREEN}""5 ---- NPM""${NOCOLOR}"
  npm -v > /dev/null 2>& 1 && echo 'npm is installed'; npm install || echo "npm isn't installed"

  echo \ && echo "$final_msg" && echo \

elif [[ $option == 2 ]]; then
  # Wordpress setup
  echo \ && echo -e "${GREEN}""1 ---- Install plugins""${NOCOLOR}"

  docker exec -i wpcli sh -c "chmod +x plugins/managePluginsDev.sh"
  docker exec -i wpcli sh -c "plugins/managePluginsDev.sh"

  echo \ && echo "$final_msg" && echo \

elif [[ $option == 3 ]]; then
  echo \ && echo -e "${GREEN}""1 ---- Update wp-config.php""${NOCOLOR}"

  docker exec -i bk_wordpress sh -c 'rm wp-config.php'

  # shellcheck disable=SC2164
  cd wp
  docker-compose down
  docker-compose up -d

  echo \ && echo "$final_msg" && echo \

elif [[ $option == 4 ]]; then
  echo \ && echo -e "${GREEN}""1 ---- Clean Debug.log""${NOCOLOR}"

  rm 'wp/wp-content/'debug.log
  touch 'wp/wp-content/'debug.log

  echo \ && echo "$final_msg"  && echo \

elif [[ $option == 5 ]]; then
  # DB
  echo \ && echo -e "${GREEN}""1 ---- Import DB""${NOCOLOR}"

  docker exec -i bk_mysql sh -c "chmod +x /tmp/db/import.sh"
  docker exec -i bk_mysql sh -c "/tmp/db/import.sh"

  echo -e "${GREEN}""2 ---- WP Rewrite flush""${NOCOLOR}"
  docker exec -i wpcli sh -c 'wp rewrite flush'

  echo \ && echo "$final_msg"  && echo \

else
  echo \ && echo "@(-_-) ============= Invalid option ============= (-_-)!" && echo \

fi

exit 0


# prueba commit