#!/bin/bash

RED="\033[1;31m"
GREEN="\033[1;32m"
NOCOLOR="\033[0m"

SSH_PARAMS="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

# Export the vars in .env into your shell:
echo -e ${GREEN}"-- BUILDING With .env file --"${NOCOLOR}
export $(egrep -v '^#' .env | xargs)

        echo -e ${GREEN}"2 - Server Deploy 🚀 - "${NOCOLOR}

        echo -e ${GREEN}"2.1 ---- Check Docker On Server ----"${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "type docker > /dev/null 2>& 1 && echo 'Docker present.' || ( echo 'Installing Docker.' ; wget -qO- https://get.docker.com/ | sh; sudo usermod -aG docker $(whoami); sudo apt update; sudo apt -y install python3-pip python-dev libffi-dev openssl gcc libc-dev make; sudo pip3 install docker-compose )"

        echo -e ${GREEN}"2.1 ---- Check Haveged On Server ----"${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "type haveged > /dev/null 2>& 1 && echo 'Haveged present.' || ( echo 'Installing Haveged.' ; sudo apt-get -y install haveged )"
        
        echo -e ${GREEN}"2.1 ---- Stop Mysql ----"${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "sudo /etc/init.d/mysql stop"

        # REF: https://lebkowski.name/docker-volumes/
        echo -e ${GREEN}"2.2 ---- Remove unused Docker Builds On Server"${NOCOLOR}
        #ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "docker ps --filter status=dead --filter status=exited -aq | xargs -r docker rm -v; docker images --no-trunc | grep '<none>' | awk '{ print $3 }' | xargs -r docker rmi; find '/var/lib/docker/volumes/' -mindepth 1 -maxdepth 1 -type d | grep -vFf <( docker ps -aq | xargs docker inspect | jq -r '.[] | .Mounts | .[] | .Name | select(.)' ) | xargs -r rm -fr"

        echo -e ${GREEN}"2.3 ---- Check SWAP on Server"${NOCOLOR}
        if free | awk '/^Swap:/ {exit !$2}'; then
            echo "Have SWAP"
        else
            echo "No SWAP - Creating:"
            ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "sudo fallocate -l 1G /swapfile"
            ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "ls -lh /swapfile"
            ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "sudo chmod 600 /swapfile"
            ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "ls -lh /swapfile"
            ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "sudo mkswap /swapfile"
            ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "sudo swapon /swapfile"
            ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "sudo swapon --show"
        fi
        
        #sed -i "s|localhost:8080|${SERVER_IP}|g" wp/db/wp.sql
        #sed -i ".original" "s|localhost:8080|${SERVER_IP}|g" wp/db/wp.sql
#
        #if [[ $OSTYPE == 'linux-gnu' ]]; then
        #    sed -i 's|localhost:8080|${SERVER_IP}|g' wp/db/wp.sql
        #elif [[ $OSTYPE == 'darwin'* ]]; then
        #    sed -i '.original' 's|localhost:8080|${SERVER_IP}|g' wp/db/wp.sql
        #else
        #    echo 'Platforn not Suported'
        #fi

        echo -e ${GREEN}"2.4 ---- Create project Folder On Server ---"${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "mkdir -p $SERVER_FOLDER"

        echo -e ${GREEN}"2.5 ---- Copy Files To Server  - " ${NOCOLOR}
        rsync -azI --exclude-from deploy_excluded_files.txt -e "ssh -i $SERVER_SSH_KEY $SSH_PARAMS" $LOCAL_PATH/.env $SERVER_SSH_USER@$SERVER_IP:$SERVER_FOLDER/.env
        rsync -azI --exclude-from deploy_excluded_files.txt -e "ssh -i $SERVER_SSH_KEY $SSH_PARAMS" $LOCAL_PATH/app/ $SERVER_SSH_USER@$SERVER_IP:$SERVER_FOLDER/app/
        rsync -azI --exclude-from deploy_excluded_files.txt -e "ssh -i $SERVER_SSH_KEY $SSH_PARAMS" $LOCAL_PATH/config/ $SERVER_SSH_USER@$SERVER_IP:$SERVER_FOLDER/config/
        #rsync -azI --exclude-from deploy_excluded_files.txt -e "ssh -i $SERVER_SSH_KEY $SSH_PARAMS" $LOCAL_PATH/data/ $SERVER_SSH_USER@$SERVER_IP:$SERVER_FOLDER/data/
        rsync -azI --exclude-from deploy_excluded_files.txt -e "ssh -i $SERVER_SSH_KEY $SSH_PARAMS" $LOCAL_PATH/plugins/ $SERVER_SSH_USER@$SERVER_IP:$SERVER_FOLDER/plugins/
        rsync -azI --exclude-from deploy_excluded_files.txt -e "ssh -i $SERVER_SSH_KEY $SSH_PARAMS" $LOCAL_PATH/db/ $SERVER_SSH_USER@$SERVER_IP:$SERVER_FOLDER/db/

        if [ "$PRODUCTION" == "true" ]
                then
                rsync -azI --exclude-from deploy_excluded_files.txt -e "ssh -i $SERVER_SSH_KEY $SSH_PARAMS" $LOCAL_PATH/docker-compose_production.yml $SERVER_SSH_USER@$SERVER_IP:$SERVER_FOLDER/docker-compose.yml
        else
                rsync -azI --exclude-from deploy_excluded_files.txt -e "ssh -i $SERVER_SSH_KEY $SSH_PARAMS" $LOCAL_PATH/docker-compose_staging.yml $SERVER_SSH_USER@$SERVER_IP:$SERVER_FOLDER/docker-compose.yml
        fi
        
        echo -e ${GREEN}"2.6 ---- Stop Docker on Server ---"${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "cd ~/$SERVER_FOLDER; docker-compose down"
​

        echo -e ${GREEN}"2.7 ---- Run Build On Server  - "${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "cd ~/$SERVER_FOLDER; docker-compose up -d"

        echo -e ${GREEN}"2.9 ---- Set permissions"${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "docker exec -i "${APPNAME}"_wordpress sh -c 'chown www-data:www-data -R wp-content'"
​
        echo -e ${GREEN}"2.8 ---- Run Composer  - "${NOCOLOR}
        #ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "cd ~/$SERVER_FOLDER; rm -rf app/understrap-bk-child/vendor/"
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "cd ~/$SERVER_FOLDER; docker-compose run --rm composer clear-cache"
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "cd ~/$SERVER_FOLDER; docker-compose run --rm composer install"
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "cd ~/$SERVER_FOLDER; docker-compose run --rm composer update"
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "cd ~/$SERVER_FOLDER; docker-compose run --rm composer dump-autoload"


        echo -e ${GREEN}"2.9 ---- Check MySql"${NOCOLOR}
​
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "type mysql > /dev/null 2>& 1 && echo 'MySQL present.' || ( echo 'Installing MySQL.' ; sudo apt install mysql-server -y )"
        
        if [ "$INSTALL" == "true" ]
                then
                echo -e ${GREEN}"2.9 ---- Initial Setup"${NOCOLOR}
                #ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'mysql --user='${MYSQL_ROOT_PASSWORD}' --password="'${MYSQL_ROOT_PASSWORD}'" --port=3306 --host=127.0.0.1 --database='${DB_NAME}' < '$SERVER_FOLDER'/db/wp.sql'
                #ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp core install --url=${SERVER_IP} --title=nelson_sobrero --admin_user=admin --admin_password=admin --admin_email=serversadmins+ns@bitskingdom.com"'
                echo -e ${GREEN}"2.9 ---- DB Update"${NOCOLOR}
                #ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp theme install /var/www/html/wp-content/themes/understrap.zip --activate"'
                #ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp search-replace 'localhost' '${SERVER_IP}' --all-tables"'

                echo -e ${GREEN}"2.9 ---- WpContent Dummy"${NOCOLOR}
                #ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "cp -R ~/$SERVER_FOLDER/db/wp-content/uploads ~/$SERVER_FOLDER/wp-content/"

        fi


        echo -e ${GREEN}"3.0 ---- Initial Plugins"${NOCOLOR}      
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli bash -c "plugins/managePlugins.sh"'

        #echo -e "${GREEN}""4.0 ---- Compile static files""${NOCOLOR}"
        #npm -v > /dev/null 2>& 1 && echo 'npm is installed'; "cd ~/$SERVER_FOLDER/app"; npm install; npm run build || echo "npm isn't installed"