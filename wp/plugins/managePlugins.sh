#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


RED='\033[1;31m'
GREEN='\033[1;32m'
NOCOLOR='\033[0m'

echo -e ${RED}"- Checking for WP-CLI... -"${NOCOLOR}

echo -e ${RED}"-- Managing Plugins - WP-CLI --"${NOCOLOR}

echo -e ${RED}"--> Deactivate all plugins before start..."${NOCOLOR}
echo "--> Deactivating all plugins"
wp plugin deactivate --all --allow-root --network

echo "--> Removing all inactive plugins"
wp plugin delete $(wp plugin list --status=inactive --field=name) --all --allow-root

echo -e ${RED}"--> Checking plugins list ..."${NOCOLOR}
while IFS="=" read -r plugin version
do 
    IFS=@ read temp custom <<< "$plugin"
    if [ -n "$custom" ]
        then
            echo "Custom Plugin -- " $custom
            if ! $( wp plugin is-installed $custom --allow-root ) ;
                then
                    echo "---> Installing from ZIP : "$custom
                    wp plugin install $DIR"/custom-plugins/"$custom".zip" --activate --allow-root  --force
                else
                    echo "--> "$custom" Already Installed"
                    wp plugin install $DIR"/custom-plugins/"$custom".zip" --activate --allow-root  --force
            fi 
        else
            echo "Plugin on WP Directory: " $plugin
            if ! $( wp plugin is-installed $plugin --allow-root ) ;
                then
                    if [ -n "$version" ]
                        then
                            echo "---> Installing: "$plugin "version "$version
                            wp plugin install $plugin --version=$version --activate --allow-root  --force
                        else
                            echo "---> Installing: "$plugin " latest version "
                            wp plugin install $plugin --activate --allow-root  --force
                    fi
                else
                    echo "--> "$plugin" Already Installed"
                    wp plugin install $plugin --activate --allow-root  --force
            fi  
    fi
done <$DIR"/"plugins

echo -e ${GREEN}"--> Plugins management finished"${NOCOLOR}
