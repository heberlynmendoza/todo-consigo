#!/bin/bash

# shellcheck disable=SC2034
RED="\033[1;31m"
GREEN="\033[1;32m"
NOCOLOR="\033[0m"

while IFS="=" read -r plugin version
do
  # shellcheck disable=SC2162
  IFS=@ read temp custom <<< "$plugin"
  if [ -n "$custom" ]
    then
    echo -e "${RED}""–––––> Installing Plugin from ZIP : $custom""${NOCOLOR}"
    wp plugin install plugins/zip/"$custom".zip --force --activate
  else
    echo -e "${RED}""–––––> Installing Plugin from WP Site : $plugin""${NOCOLOR}"
    wp plugin install "$plugin" --force --activate
  fi
done <"plugins/plugins.dev"
