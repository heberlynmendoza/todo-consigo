#!/bin/bash

# shellcheck disable=SC2034
RED="\033[1;31m"
GREEN="\033[1;32m"
NOCOLOR="\033[0m"

echo -e "${GREEN}Run SQL${NOCOLOR}"
mysql --user=root --password="root" --database=wp_bk < "/tmp/db/wp.sql"
mysql --user=root --password="root" --database=wp_bk -e "SELECT count(*) as count FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE';"
