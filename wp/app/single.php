<?php
/**
 * The template for displaying all single posts
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

    <div class="wrapper" id="single-wrapper">
        <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
            <div class="row">

				<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

                <main class="site-main" id="main">

					<?php
					while ( have_posts() ) {
						the_post();
						get_template_part( 'loop-templates/content', 'single' );
						understrap_post_nav();

						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}
					}
					?>

                </main>

				<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

            </div>
        </div>
    </div>

<?php
get_footer();
