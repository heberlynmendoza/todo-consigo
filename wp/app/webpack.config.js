const path = require('path');

const webpack = require('webpack');
const merge = require('merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')

function recursiveIssuer(m, c) {
	const issuer = c.moduleGraph.getIssuer(m);

	if (issuer) {
		return recursiveIssuer(issuer, c);
	}

	const chunks = c.chunkGraph.getModuleChunks(m);

	chunks.forEach((chunk) => {
		return chunk.name;
	});

	return false;
}

const globalConfig = ({env}) => ({
	entry: {
		theme: [
			path.resolve(__dirname, './src/js/theme.js'),
			path.resolve(__dirname, './src/sass/theme.scss'),
		]
	},

	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, './static/js'),
	},

	optimization: {
		splitChunks: {
			cacheGroups: {
				themeStyles: {
					name: 'styles_theme',
					test: (m, c, entry = 'theme') =>
						m.constructor.name === 'CssModule' &&
						recursiveIssuer(m, c) === entry,
					chunks: 'all',
					enforce: true,
				},
			},
		},
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: ['babel-loader'],
			},
			{
				test: /\.(sa|sc|c)ss$/,
				exclude: /node_modules/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							url: false,
							sourceMap: true,
						},
					},
					{
						loader: 'sass-loader',
						options: {
							implementation: require('sass'),
							sourceMap: true,
						},
					},
				],
			},
		],
	},

	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
		}),

		new MiniCssExtractPlugin({
			filename: '../css/[name].min.css',
		}),
	],
})

module.exports = (env, argv) => {
	const config = globalConfig({
		env: argv.mode,
	});

	if (argv.mode === 'development') {
		const devConfig = {
			stats: {
				hash: false,
				version: false,
				timings: false,
				children: false,
				errors: false,
				errorDetails: false,
				warnings: false,
				chunks: false,
				modules: false,
				moduleTrace: false,
				reasons: false,
				source: false,
			},
		};

		// Add more plugins to development
		Array.prototype.push.apply(config.plugins, [
			new webpack.DefinePlugin({
				'ENV': JSON.stringify(argv.mode),
			}),
			new FriendlyErrorsWebpackPlugin(),
		]);

		return merge(config, devConfig)
	}

	if (argv.mode === 'production') {
		const prodConfig = {};

		// Add more plugins to production
		Array.prototype.push.apply(config.plugins, []);

		return merge(config, prodConfig)
	}
}