<?php
/**
 * The template for displaying search results pages
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

    <div class="wrapper" id="search-wrapper">
        <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
            <div class="row">

				<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

                <main class="site-main" id="main">

					<?php if ( have_posts() ) : ?>
                        <header class="page-header">
                            <h1 class="page-title">
								<?php
								printf( esc_html__( 'Search Results for: %s', 'understrap' ), '<span>' . get_search_query() . '</span>' );
								?>
                            </h1>
                        </header>

						<?php
						while ( have_posts() ) :
							the_post();
							get_template_part( 'loop-templates/content', 'search' );
						endwhile;
						?>

					<?php else : ?>
						<?php get_template_part( 'loop-templates/content', 'none' ); ?>
					<?php endif; ?>

                </main><!-- #main -->

				<?php understrap_pagination(); ?>

				<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

            </div>
        </div>
    </div>
<?php
get_footer();
