import jQuery from 'jquery';

(function ($) {
	$(window).on('load', function () {
		$(function () {
			function NS_Theme_Checkout() {
				const _self = this;
				_self.ns_checkout_url = ns_checkout_ajax_var.url;
				_self.ns_checkout_nonce = ns_checkout_ajax_var.nonce;
				_self.notice = $('#ns_woocommerce-checkout__errors');

				_self.steps();
				_self.customizeCheckoutPage();
			}

			NS_Theme_Checkout.prototype.steps = function () {
				const params = new URLSearchParams(window.location.search);
				const step = parseInt(params.get('step'));

				if (step && !isNaN(step)) {
					switch (step) {
						case 1:
							this.handleShippingForm();
							break;
						case 2:
							this.handleShippingProvider();
							break;
						case 3:
							this.step3();
							break;
						default:
							break;
					}
				}
			};

			NS_Theme_Checkout.prototype.handleShippingForm = function () {
				const _self = this;

				$('#step-1').on('submit', function (e) {
					e.preventDefault();
					_self.notice.html('');

					const formData = $(this).serializeArray();
					const request = _self.WPRequest('ns_shipping', formData);

					request.done((res) => {
						if (res.success) {
							window.location.href = _self.createUrl(res.data);
						} else {
							if (typeof res.data === 'string') {
								_self.createNotice('error', res.data);
							} else {
								$.each(res.data, function ($index, $error) {
									_self.createNotice('error', $error);
								});
							}
						}
					});
				});
			};

			NS_Theme_Checkout.prototype.handleShippingProvider = function () {
				const _self = this;

				$('#step-2').on('submit', function (e) {
					e.preventDefault();
					_self.notice.html('');

					const formData = $(this).serializeArray();

					const request = _self.WPRequest('ns_shipping_provider', formData);

					request.done((res) => {
						if (res.success) {
							window.location.href = _self.createUrl(res.data);
						} else {
							_self.createNotice('error', res.data);
						}
					});
				});
			};

			NS_Theme_Checkout.prototype.step3 = function () {
			};

			NS_Theme_Checkout.prototype.WPRequest = function (action, data) {
				const _self = this;

				data.push(
					{name: 'nonce', value: _self.ns_checkout_nonce},
					{name: 'action', value: action},
				);

				return $.ajax({
					method: 'POST',
					url: _self.ns_checkout_url,
					data: data,
					dataType: 'json',
					encode: true,
					beforeSend: function () {
						$('#btn-submit').prop('disabled', true);
					},
					complete: function (res) {
						$('#btn-submit').prop('disabled', false);
					},
				});
			}

			NS_Theme_Checkout.prototype.createUrl = function (params) {
				if (params.constructor === Object) {
					const urlParams = new URLSearchParams();

					Object.keys(params).map((key) => {
						urlParams.set(key, params[key]);
					});

					return `${window.location.origin}${
						window.location.pathname
					}?${urlParams.toString()}`;
				}

				throw new Error('Params is not an Object');
			};

			NS_Theme_Checkout.prototype.createNotice = function (type, message) {
				const _self = this;
				const html = `<div class="notice woocommerce-${type}" style="display: none">${message}</div>`;

				_self.notice.append($(html));
				_self.notice.find('.notice').fadeIn();
			};

			NS_Theme_Checkout.prototype.customizeCheckoutPage = function () {
				const wrapper = $('#wc_payment_form .col2-set');
				const row = $('<div class="row payment_form_row"></div>');
				const footer = $('<div class="d-flex justify-content-end align-items-center payment_form_footer"></div>');

				// Body
				wrapper.find('.col-1').removeClass('col-1').addClass('col-md-6 payment_form_col');
				wrapper.find('.col-2').removeClass('col-2').addClass('col-md-6 payment_form_col');
				$('.payment_form_col').wrapAll(row);

				// Footer
				wrapper.find('#btnCheckoutCT').addClass('payment_form_footer_button').appendTo('#btnAddCard');
				wrapper.find('#btnAddCard').addClass('payment_form_footer_button');
				$('.payment_form_footer_button').wrapAll(footer);

				// Col 1
				wrapper.find('#pagosweb_tipodoc').addClass('form-control').parent().prev('label').addClass('payment_form_label');
				wrapper.find('#pagosweb_documento').addClass('form-control').parent().prev('label').addClass('payment_form_label');
				wrapper.find('#pagosweb_telefono').addClass('form-control').parent().prev('label').addClass('payment_form_label');

				// Col 2
				wrapper.find('#sp_installments').addClass('form-control').prev('h5').text('Cuotas').addClass('payment_form_label');
				wrapper.find('#favCardDiv').children('br').remove();
				wrapper.find('#favCardDiv').children('p').css({
					fontSize: '.8rem',
				});
				wrapper.find('#sp_customer_cards').children('h4').addClass('payment_form_label').next().css({margin: 0});
				wrapper.find('#sp_pmedia').parent().prepend('<h4>Medio</h4>').children('h4').addClass('payment_form_label');
				wrapper.find('#sp_pmedia').parent().append('<div id="sp_select_card"></div>')
				wrapper.find('#sp_pmedia').addClass('form-control');
				wrapper.find('#card').addClass('form-control');
				$('#selectCard').appendTo('#sp_select_card');

				wrapper.fadeIn();
			}

			new NS_Theme_Checkout();
		});
	});
})(jQuery);