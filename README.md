#### Local Deploy:
1. Ir a la carpeta `wp/` y duplicar el archivo `.env.example` luego renombrarlo como `.env`
2. Crear el archivo `debug.log` dentro de la carpeta `wp/wp-content/`
3. Desde la terminal ubicarse en `wp/`
4. Despliegue de contenedores: `$ docker-compose up -d`
5. Esperar a que los contenedores se desplieguen completamente; se puede verificar con `$ docker ps` o `$ docker-compose logs -f `
6. Instalación de paquetes Composer via docker: `$ docker-compose run composer install`
7. Desde la terminal ubicarse en `wp/app/`
8. Instalación de paquetes NPM: `$ npm install`
9. Ir a `http://localhost:8080` y la respuesta debe ser la instalación de wordpress
10. Importar la base de datos desde PHP-MYADMIN, ir a `http://localhost:9090`, ingresar y selecionar la base datos llamada `wp_app`
11. Selecionar la opción `importar` e importamos el archivo ubicado en `wp/db/wp.sql`
12. Instalación de plugins base: Primero ejecutamos `docker exec -i wpcli sh -c "chmod +x plugins/managePluginsDev.sh"` y luego `docker exec -i wpcli sh -c "plugins/managePluginsDev.sh"`
13. Si el paso anterior da error, podemos instalar los plugins de manera manual desde el administrador de wordpress

##### Nota: Para ejecutar cualquier comando relacionado a `docker-compose` debemos estar siempre ubicados en la carpeta `wp/`
##### Nota: Para ejecutar cualquier comando relacionado a `npm` debemos estar siempre ubicados en la carpeta `wp/app/`

#### Compilar Sass y JS:
1. Desde la terminal ubicarse en `wp/app/`
2. Ejecutar: `$ npm run dev`

#### Configuración del Server Mail:
URL: `http://localhost:8025`
1. Se debe usar el plugin WP Mail SMTP para confgiurar la salida de emails
2. Ir a `http://localhost:8080/wp-admin/admin.php?page=wp-mail-smtp` y en la opción de Mailer usar Other STMP
3. Configuración:
- SMTP Host: mailhog
- Encryption: None
- SMTP Port: 1025
- Auto TLS: On
- Authentication: Off

#### WP Access - Local:
- user: `admin`
- password: `BitsKingdom1234`

#### PHP My Admin Access - Local:
- user: `root`
- password: `root`
